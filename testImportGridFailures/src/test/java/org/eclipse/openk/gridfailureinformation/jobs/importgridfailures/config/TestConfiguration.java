/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.jobs.importgridfailures.config;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.openk.gridfailureinformation.jobs.importgridfailures.TestImportGridFailuresApplication;
import org.eclipse.openk.gridfailureinformation.jobs.importgridfailures.service.ImportService;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.messaging.Message;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

@EntityScan(basePackageClasses = TestImportGridFailuresApplication.class)
public class TestConfiguration {
    @Value("${spring.rabbitmq.host}")
    private String rabbitMqHost;

    @Value("${spring.rabbitmq.username}")
    private String rabbitMqUsername;

    @Value("${spring.rabbitmq.password}")
    private String rabbitMqPassword;

    @Bean
    public RabbitTemplate rabbitTemplate() {
        var factory = new CachingConnectionFactory();
        factory.setHost(rabbitMqHost);
        factory.setUsername(rabbitMqUsername);
        factory.setPassword(rabbitMqPassword);
        return new RabbitTemplate(factory);
    }

    @Bean
    public EventProducerConfig eventProducerConfig() {
        return new EventProducerConfig(rabbitTemplate());
    }

    @Bean
    public DirectChannel messageChannel() {
        DirectChannel dc = new DirectChannel();

        var channelSpy = spy(dc);

        doReturn(true).when(channelSpy).send(any(Message.class));

        channelSpy.subscribe(eventProducerConfig().messageHandler());

        return channelSpy;
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    @Bean
    public ImportService importService() {
        return new ImportService(messageChannel(), objectMapper());
    }
}

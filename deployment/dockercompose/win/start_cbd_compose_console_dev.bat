@echo off

rem Setzen Sie das Root-Verzeichnis, in dem sich die Docker-Compose-Projekte befinden
set "ROOT_DIR=%~dp0.."

rem Start Projekt 1 in einer neuen Konsole
start "gfi" cmd /c "cd /d %ROOT_DIR%/cbd && docker-compose -f docker-compose-dev.yml  up"
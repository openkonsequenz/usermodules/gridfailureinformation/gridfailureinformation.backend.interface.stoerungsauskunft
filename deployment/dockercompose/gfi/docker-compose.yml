version: '3.5'

services:
  rabbitmq:
    image: rabbitmq:3-management-alpine
    networks:
      - openknet
    environment:
      RABBITMQ_DEFAULT_USER: ${RMQ_USER}
      RABBITMQ_DEFAULT_PASS: ${RMQ_PASSWORD}
    ports:
      - "5672:5672"
      - "15672:15672"
    healthcheck:
      test: rabbitmq-diagnostics -q ping
      interval: 3s
      timeout: 30s
      retries: 3
      start_period: 6s

############# Database Init #############

  gfidb-init:
    image: postgres:9.6.22-alpine3.14
    environment:
      PGHOST: ${APP_DB_HOST}
      PGPORT: ${APP_DB_PORT}
      PGUSER: postgres
      PGPASSWORD: postgres
    command:
    - /bin/sh
    - -c
    - >
      echo Waiting for DB to be ready...;
      while ! pg_isready >dev/null; do sleep 3; done;
      echo DB is ready and accepting conncetions;
      sleep 3;
      echo Creating database ${APP_DBNAME} if not exists;
      psql -tc "SELECT 1 FROM pg_database WHERE datname = '${APP_DBNAME}'" | grep -q 1 || psql -U postgres -c "CREATE DATABASE \"${APP_DBNAME}\"";
      echo Creating role ${APP_DB_ROLE} if not exists;
      psql -tc "SELECT 1 FROM pg_user WHERE usename = '${APP_DB_ROLE}'" | grep -q 1 || psql -c "CREATE USER \"${APP_DB_ROLE}\" WITH PASSWORD '${APP_DB_PASSWORD}'";
      echo Job finished;
    networks:
      - openknet

  gfidb-migration:
    #image: registry.gitlab.com/openkonsequenz/usermodules/gridfailureinformation/gridfailureinformation.backend/tag/gfi-flyway:2.0.1
    image: registry.gitlab.com/openkonsequenz/usermodules/gridfailureinformation/gridfailureinformation.backend/develop/gfi-flyway:latest
    environment:
      FLYWAY_URL: jdbc:postgresql://${APP_DB_HOST}:${APP_DB_PORT}/${APP_DBNAME}
      FLYWAY_USER: ${APP_DB_ROLE}
      FLYWAY_PASSWORD: ${APP_DB_PASSWORD}
    command: info migrate info
    networks:
      - openknet
    # depends_on:
    #   gfidb-init:
    #     condition: service_completed_successfully

############# Backend #############

  backend-service:
    #image: registry.gitlab.com/openkonsequenz/usermodules/gridfailureinformation/gridfailureinformation.backend/tag/gfi-main:2.0.1
    image: registry.gitlab.com/openkonsequenz/usermodules/gridfailureinformation/gridfailureinformation.backend/develop/gfi-main:latest
    networks:
       - openknet
    environment:
      SPRING_CONFIG_LOCATION: 'file:/config/'
      #SPRING_PROFILES_ACTIVE: dev
      RMQ_HOST: ${RMQ_HOST}
      RMQ_USER: ${RMQ_USER}
      RMQ_PASSWORD: ${RMQ_PASSWORD}
      DATASOURCE_URL: jdbc:postgresql://${APP_DB_HOST}:${APP_DB_PORT}/${APP_DBNAME}
      KEYCLOAK_PASSWORD_TECHUSER: ${KEYCLOAK_PASSWORD_TECHUSER:-admin}
      APP_DB_ROLE: ${APP_DB_ROLE}
      APP_DB_PASSWORD: ${APP_DB_PASSWORD}
      APP_DBNAME: ${APP_DBNAME}
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.gfibe.rule=Host(`main.gfi.${ROOT_DOMAIN:-localhost}`) && PathPrefix(`/api`)"
      - "traefik.http.middlewares.api-strip.stripprefix.prefixes=/api"
      - "traefik.http.routers.gfibe.middlewares=api-strip"
      - "traefik.http.routers.gfibe.entrypoints=web"
      # spring server port
      - "traefik.http.services.gfibe.loadbalancer.server.port=9156"
    ports:
      - "9156:9156"
    volumes:
      - ./configBackend/configMain/application.yml:/config/application_kube.yml
    depends_on:
      gfidb-migration:
        condition: service_completed_successfully
      rabbitmq:
        condition: service_healthy

  saris-service:    
    #image: registry.gitlab.com/openkonsequenz/usermodules/gridfailureinformation/gridfailureinformation.backend.interface.saris/tag/gfi-saris-interface:2.2.0
    registry.gitlab.com/openkonsequenz/usermodules/gridfailureinformation/gridfailureinformation.backend.interface.saris/develop/gfi-saris-interface:latest
    networks:
       - openknet
    environment:
      SPRING_CONFIG_LOCATION: 'file:/config/'
      RMQ_HOST: ${RMQ_HOST}
      RMQ_USER: ${RMQ_USER}
      RMQ_PASSWORD: ${RMQ_PASSWORD}
      GFI_SARIS_USERNAME: ${GFI_SARIS_USERNAME}
      GFI_SARIS_PASSWORD:  ${GFI_SARIS_PASSWORD}
      GFI_MANUAL_ENDPOINTS_USERNAME: ${GFI_MANUAL_ENDPOINTS_USERNAME}
      GFI_MANUAL_ENDPOINTS_PASSWORD: ${GFI_MANUAL_ENDPOINTS_PASSWORD}
    command: ["sh", "-c", "/usr/src/cbd/entrypoint.sh"]
    #command: sh -c "chmod +x /usr/src/cbd/entrypoint.sh && /usr/src/cbd/entrypoint.sh"
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.gfisaris.rule=Host(`saris.gfi.${ROOT_DOMAIN:-localhost}`)"
      - "traefik.http.routers.gfisaris.entrypoints=web"
      - "traefik.http.services.gfisaris.loadbalancer.server.port=9158"
    ports:
      - "9158:9158"
    volumes:
      - ./configBackend/configSaris/application.yml:/config/application.yml
      - ./configBackend/configSaris/certs:/opt/import_certs
    depends_on:
      gfidb-migration:
        condition: service_completed_successfully
      rabbitmq:
        condition: service_healthy


############# Frontend #############

  fe-main:
    #image: registry.gitlab.com/openkonsequenz/usermodules/gridfailureinformation/gridfailureinformation.frontend/tag/gfi-main:2.0.1
    #image: registry.gitlab.com/openkonsequenz/usermodules/gridfailureinformation/gridfailureinformation.frontend/tag/gfi-main:3.1.0
    image: registry.gitlab.com/openkonsequenz/usermodules/gridfailureinformation/gridfailureinformation.frontend/develop/gfi-main:latest
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.gfimain.rule=Host(`main.gfi.${ROOT_DOMAIN:-localhost}`)"
      - "traefik.http.routers.gfimain.entrypoints=web"
    networks:
      - openknet
    volumes:
      - ./configFrontend/configMain:/usr/share/nginx/html/config
    depends_on:
      gfidb-migration:
        condition: service_completed_successfully

networks:
  openknet:
    name: openknet
    driver: bridge

volumes:
  gfidbvol:

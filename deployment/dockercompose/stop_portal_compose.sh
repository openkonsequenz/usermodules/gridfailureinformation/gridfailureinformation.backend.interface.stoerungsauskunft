#!/bin/sh

# Ermittle das Verzeichnis, in dem sich das Shell-Script befindet
ROOT_DIR=$(cd "$(dirname "$0")" && pwd)

# Infrastructure
docker-compose -f "$ROOT_DIR/infrastructure/traefik/docker-compose.yml" down

# Projekt 2
docker-compose -f "$ROOT_DIR/portal/docker-compose.yml" down

# Projekt 3
docker-compose -f "$ROOT_DIR/cbd/docker-compose.yml" down

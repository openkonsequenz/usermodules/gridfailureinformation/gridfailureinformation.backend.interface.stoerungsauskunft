/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.gridfailureinformation.config.auth;

import feign.Response;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpServletResponseWrapper;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.api.AuthNAuthApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Component
@Log4j2
public class JwtTokenValidationFilter extends OncePerRequestFilter {
    @Lazy
    private final AuthNAuthApi authNAuthApi;

    @Value("${jwt.useStaticJwt}")
    private boolean useStaticJwt;

    @Value("${jwt.tokenHeader}")
    private String tokenHeader;

    public JwtTokenValidationFilter(AuthNAuthApi authNAuthApi) {
        this.authNAuthApi = authNAuthApi;
    }

    @Override
    protected void doFilterInternal(@NonNull HttpServletRequest request, @NonNull HttpServletResponse response, @NonNull FilterChain chain) throws IOException, ServletException {
        String authenticationHeader = useStaticJwt ? null : request.getHeader(this.tokenHeader);

            if (authenticationHeader != null) {
                final String bearerTkn = authenticationHeader.replace("Bearer ", "");
                Response res = authNAuthApi.isTokenValid(bearerTkn);
                if (res.status() != HttpStatus.OK.value()) {
                    final HttpServletResponseWrapper wrapper = new HttpServletResponseWrapper(response);
                    wrapper.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Token expired or not valid");
                    chain.doFilter(request, wrapper.getResponse());

                    return;
                }
            }
            chain.doFilter(request, response);
    }
}

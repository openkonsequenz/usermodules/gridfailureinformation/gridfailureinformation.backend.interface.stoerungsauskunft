/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.constants;

public final class Constants {
    public static final String VOLTAGE_LEVEL_NS = "NS";
    public static final String VOLTAGE_LEVEL_MS = "MS";
    public static final String VOLTAGE_LEVEL_HS = "HS";
    public static final String PRESSURE_LEVEL_ND = "ND";
    public static final String PRESSURE_LEVEL_MD = "MD";
    public static final String PRESSURE_LEVEL_HD = "HD";


    public static final String DB_VERSION_NOT_PRESENT = "DB-Version-not_present";

    public static final String INTERNAL_SHORT = "I";
    public static final String EXTERNAL_SHORT = "E";
    public static final String FAILURE_INFO_UUID_NOT_EXISTING = "failure.info.uuid.not.existing";
    public static final String STATUS_NOT_EXISTING = "status.not.existing";
    public static final String PUB_STATUS_VEROEFFENTLICHT = "veröffentlicht";
    public static final String PUB_STATUS_UNVEROEFFENTLICHT = "nicht veröffentlicht";
    public static final String PUB_STATUS_ZURUECKGEZOGEN = "zurückgezogen";
    public static final String EXT_STATUS_IN_WORK = "in Bearbeitung";
    public static final String EXT_STATUS_FINISHED = "beendet";
    public static final String EXT_STATUS_PLANNED = "geplant";
    public static final String TK = "TK";
    public static final String F = "F";
    public static final String W = "W";
    public static final String G = "G";
    public static final String S = "S";
    public static final String CHANNEL_NOT_EXISTING = "channel.not.existing";

    public static final String CHANNEL_TPYE_LONG_MAIL = "longMail";
    public static final String CHANNEL_TPYE_SHORT_MAIL = "shortMail";
    public static final String CHANNEL_TPYE_STOERUNGSAUSLKUNFT_DE = "stoerungsauskunftde";
    public static final String CHANNEL_TPYE_PUBLICATION_OWNDMZ = "webcomponent";

    public static final String FREETEXT_ADDRESS_TYPE="freetext";

    public static final String LOCATION_TYPE_ADDRESS="address";
    public static final String LOCATION_TYPE_MAP="map";
    public static final String LOCATION_TYPE_STATION="station";

    private Constants() {
        // empty Constructor for the sake of SONAR
    }
}

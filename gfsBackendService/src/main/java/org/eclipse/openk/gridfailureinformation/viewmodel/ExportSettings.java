package org.eclipse.openk.gridfailureinformation.viewmodel;

import lombok.Data;

import java.util.List;

@Data
public class ExportSettings {

    private String password;
    private FESettingsDto settings;
}

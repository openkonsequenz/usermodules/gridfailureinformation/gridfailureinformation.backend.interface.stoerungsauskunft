/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.service;

import feign.Response;
import org.eclipse.openk.gridfailureinformation.api.AuthNAuthApi;
import org.eclipse.openk.gridfailureinformation.api.ContactApi;
import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.exceptions.NotFoundException;
import org.eclipse.openk.gridfailureinformation.exceptions.OperationDeniedException;
import org.eclipse.openk.gridfailureinformation.mapper.DistributionGroupMemberMapper;
import org.eclipse.openk.gridfailureinformation.model.JwtToken;
import org.eclipse.openk.gridfailureinformation.model.TblDistributionGroup;
import org.eclipse.openk.gridfailureinformation.model.TblDistributionGroupMember;
import org.eclipse.openk.gridfailureinformation.repository.DistributionGroupMemberRepository;
import org.eclipse.openk.gridfailureinformation.repository.DistributionGroupRepository;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.DistributionGroupMemberDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
class DistributionGroupServiceMemberTest {
    @Value("${jwt.staticJwt}")
    private String staticJwt;

    @Autowired
    private AuthNAuthApi authNAuthApi;

    @Autowired
    private Response testResponse;

    @Autowired
    private DistributionGroupMemberService distributionGroupMemberService;

    @Autowired
    private DistributionGroupMemberMapper distributionGroupMemberMapper;

    @Autowired
    private DistributionGroupMemberRepository distributionGroupMemberRepository;

    @Autowired
    private DistributionGroupRepository distributionGroupRepository;

    @Autowired
    private ContactApi contactApi;

    @BeforeEach
    public void setup() {
        var jwtToken = new JwtToken();
        jwtToken.setAccessToken(staticJwt);

        // Using thenReturn for consistent behavior
        when(authNAuthApi.login(any())).thenReturn(jwtToken);
        when(authNAuthApi.logout(any())).thenReturn(testResponse);
    }

    @Test
    void shouldGetAllDistributionGroupMembers() {
        List<TblDistributionGroupMember> groupMembers = MockDataHelper.mockDistributionGroupMemberList();
        when(distributionGroupMemberRepository.findAll()).thenReturn(groupMembers);
        List<DistributionGroupMemberDto> groupMemberDtoList = distributionGroupMemberService.getDistributionGroupMembers();

        assertEquals(groupMemberDtoList.size(), groupMembers.size());
        assertEquals(2, groupMemberDtoList.size());
        assertEquals(groupMemberDtoList.get(1).getUuid(), groupMembers.get(1).getUuid());
    }

    @Test
    @WithMockUser(username = "admin", authorities = { "ADMIN", "USER" })
    void shouldGetAllMembersOfASingleDistributionGroup() {
        List<TblDistributionGroupMember> groupMembers = MockDataHelper.mockDistributionGroupMemberList();
        when(distributionGroupMemberRepository.findByTblDistributionGroupUuid(any(UUID.class))).thenReturn(groupMembers);
        List<DistributionGroupMemberDto> groupMemberDtoList = distributionGroupMemberService.getMembersByGroupId(groupMembers.get(1).getTblDistributionGroup().getUuid());

        assertEquals(groupMemberDtoList.size(), groupMembers.size());
        assertEquals(2, groupMemberDtoList.size());
        assertEquals(groupMemberDtoList.get(1).getUuid(), groupMembers.get(1).getUuid());
    }

    @Test
    void shouldMapEmptyPlzListCorrectly() {
        TblDistributionGroupMember tbl1 = MockDataHelper.mockTblDistributionGroupMember();
        tbl1.setPostcodes(null);
        assertTrue(distributionGroupMemberMapper.toDistributionGroupMemberDto(tbl1).getPostcodeList().isEmpty());

        TblDistributionGroupMember tbl2 = MockDataHelper.mockTblDistributionGroupMember();
        tbl2.setPostcodes("");
        assertTrue(distributionGroupMemberMapper.toDistributionGroupMemberDto(tbl2).getPostcodeList().isEmpty());

        TblDistributionGroupMember tbl3 = MockDataHelper.mockTblDistributionGroupMember();
        tbl3.setPostcodes("5,3,1");
        DistributionGroupMemberDto dto3 = distributionGroupMemberMapper.toDistributionGroupMemberDto(tbl3);
        assertEquals("1", dto3.getPostcodeList().get(0));
        assertEquals("3", dto3.getPostcodeList().get(1));
        assertEquals("5", dto3.getPostcodeList().get(2));

        List<String> testList = new LinkedList<>();
        testList.add(null);
        testList.add("");
        testList.add("32");
        testList.add("23");

        dto3.setPostcodeList(testList);
        TblDistributionGroupMember tblResult = distributionGroupMemberMapper.toTblDistributionGroupMember(dto3);
        assertEquals( "23,32", tblResult.getPostcodes());
    }

    @Test
    @WithMockUser(username = "admin", authorities = { "ADMIN", "USER" })
    void shouldFindASingleDistributionGroupMember() {
        TblDistributionGroupMember groupMember = MockDataHelper.mockTblDistributionGroupMember();
        when(distributionGroupMemberRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(groupMember));
        DistributionGroupMemberDto dto = distributionGroupMemberService.getMemberByUuid(groupMember.getTblDistributionGroup().getUuid(), UUID.randomUUID());

        assertEquals(groupMember.getUuid(), dto.getUuid());
        assertEquals("12345", dto.getPostcodeList().get(0));
        assertEquals("54321", dto.getPostcodeList().get(1));
    }

    @Test
    void shouldHandleLoadFileWithNotFoundException() {
        when(distributionGroupRepository.findByUuid(any(UUID.class))).thenReturn(Optional.empty());
        UUID uuid = UUID.randomUUID();
        assertThrows( NotFoundException.class, () -> distributionGroupMemberService.handleLoadFile(uuid));
    }

    @Test
    @WithMockUser(username = "admin", authorities = { "ADMIN", "USER" })
    void shouldHandleLoadFileCorrectly() {
        TblDistributionGroup tblGroup = MockDataHelper.mockTblDistributionGroup();
        tblGroup.setName("TästMy123[]{ ");
        when(distributionGroupRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(tblGroup));

        List<TblDistributionGroupMember> tblMembers=MockDataHelper.mockDistributionGroupMemberList();
        when(distributionGroupMemberRepository.findByTblDistributionGroupUuid(any(UUID.class))).thenReturn(tblMembers);

        when(contactApi.findContact(any(UUID.class), anyString())).thenReturn(null);

        ResponseEntity<Resource> response = distributionGroupMemberService.handleLoadFile(UUID.randomUUID());
        assertTrue(response.getStatusCode().is2xxSuccessful());
    }

    @Test
    void shouldThrowExceptionWhenFailureNotFound() {
        TblDistributionGroupMember groupMember = MockDataHelper.mockTblDistributionGroupMember();
        when(distributionGroupMemberRepository.findByUuid(any(UUID.class))).thenReturn(Optional.empty());
        UUID uuid = UUID.randomUUID();
        UUID dgMemberUuid = groupMember.getTblDistributionGroup().getUuid();
        assertThrows(NotFoundException.class,
                () -> distributionGroupMemberService.getMemberByUuid(dgMemberUuid, uuid));
    }

    @Test
    void shouldInsertADistributionGroupMember() {
        DistributionGroupMemberDto memberDto = MockDataHelper.mockDistributionGroupMemberDto();
        TblDistributionGroupMember tblDistributionGroupMember = MockDataHelper.mockTblDistributionGroupMember();

        TblDistributionGroup tblDistributionGroup = MockDataHelper.mockTblDistributionGroup();

        when(distributionGroupMemberRepository.save(any(TblDistributionGroupMember.class))).thenReturn(tblDistributionGroupMember);
        when(distributionGroupRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(tblDistributionGroup));

        DistributionGroupMemberDto savedDto = distributionGroupMemberService.insertDistributionGroupMember(UUID.randomUUID(), memberDto);

        assertNotNull(savedDto.getUuid());
        assertEquals(savedDto.getUuid(), tblDistributionGroupMember.getUuid());
        assertEquals("12345,54321",
                tblDistributionGroupMember.getPostcodes());
    }

    @Test
    void shouldNotInsertMember_Exception_Member_Exists() {
        DistributionGroupMemberDto memberDto = MockDataHelper.mockDistributionGroupMemberDto();
        TblDistributionGroupMember tblDistributionGroupMember = MockDataHelper.mockTblDistributionGroupMember();

        TblDistributionGroup tblDistributionGroup = MockDataHelper.mockTblDistributionGroup();

        when(distributionGroupRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(tblDistributionGroup));
        when(distributionGroupMemberRepository.countByDistributionGroupIdAndContactId(anyLong(),any(UUID.class))).thenReturn(4L);
        when(distributionGroupMemberRepository.save(any(TblDistributionGroupMember.class))).thenReturn(tblDistributionGroupMember);
        UUID uuid = UUID.randomUUID();
        assertThrows(OperationDeniedException.class, () -> distributionGroupMemberService.insertDistributionGroupMember(uuid, memberDto));
    }

    @Test
    void shouldDeleteDistributionGroupMember() {
        TblDistributionGroupMember tblDistributionGroupMember = MockDataHelper.mockTblDistributionGroupMember();
        TblDistributionGroup tblDistributionGroup = MockDataHelper.mockTblDistributionGroup();

        when(distributionGroupMemberRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(tblDistributionGroupMember));
        when(distributionGroupRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(tblDistributionGroup));
        Mockito.doNothing().when(distributionGroupMemberRepository).delete( isA( TblDistributionGroupMember.class ));

        distributionGroupMemberService.deleteDistributionGroupMember(tblDistributionGroupMember.getTblDistributionGroup().getUuid(), tblDistributionGroupMember.getUuid());

        Mockito.verify(distributionGroupMemberRepository, times(1)).delete( tblDistributionGroupMember );
    }


    @Test
    void shouldUpdateDistributionGroupMember() {
        DistributionGroupMemberDto memberDto = MockDataHelper.mockDistributionGroupMemberDto();
        TblDistributionGroupMember tblDistributionGroupMember = MockDataHelper.mockTblDistributionGroupMember();
        TblDistributionGroup tblDistributionGroup = MockDataHelper.mockTblDistributionGroup();

        when(distributionGroupMemberRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(tblDistributionGroupMember));
        when(distributionGroupRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(tblDistributionGroup));
        when(distributionGroupMemberRepository.save(any(TblDistributionGroupMember.class))).thenReturn(tblDistributionGroupMember);
        DistributionGroupMemberDto savedMemberDto = distributionGroupMemberService.updateGroupMember(UUID.randomUUID(), memberDto);

        assertEquals(tblDistributionGroupMember.getUuid(), savedMemberDto.getUuid());
    }

    @Test
    void shouldNotUpdateMember_Exception_GroupNotFound() {
        DistributionGroupMemberDto memberDto = MockDataHelper.mockDistributionGroupMemberDto();
        TblDistributionGroupMember tblDistributionGroupMember = MockDataHelper.mockTblDistributionGroupMember();

        when(distributionGroupMemberRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(tblDistributionGroupMember));
        when(distributionGroupRepository.findByUuid(any(UUID.class))).thenReturn(Optional.empty());
        when(distributionGroupMemberRepository.save(any(TblDistributionGroupMember.class))).thenReturn(tblDistributionGroupMember);

        when(distributionGroupMemberRepository.save(any(TblDistributionGroupMember.class)))
                .then((Answer<TblDistributionGroupMember>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblDistributionGroupMember) args[0];
                });
        UUID uuid = UUID.randomUUID();
        assertThrows(NotFoundException.class, () -> distributionGroupMemberService.updateGroupMember(uuid, memberDto));
    }
}

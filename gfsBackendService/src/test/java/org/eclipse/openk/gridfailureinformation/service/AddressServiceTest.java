/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.service;

import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.model.TblAddress;
import org.eclipse.openk.gridfailureinformation.repository.AddressRepository;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.AddressDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.HousenumberUuidDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
class AddressServiceTest {
    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private AddressService addressService;

    @Test
    void shouldGetAddressByUuid() {
        UUID uuid = UUID.randomUUID();
        TblAddress adr = MockDataHelper.mockTblAddress();
        when(addressRepository.findByUuid(uuid)).thenReturn(Optional.of(adr));

        AddressDto dto = addressService.getAdressByUuid(uuid);

        assertEquals(adr.getUuid(), dto.getUuid());
        assertEquals(adr.getDistrict(), dto.getDistrict());
    }

    @Test
    void shouldGetAllAddresses() {
        Optional<String> branchOptional = Optional.empty();
        List<TblAddress> addresses = MockDataHelper.mockAddressList();
        when(addressRepository.findAll()).thenReturn(addresses);
        List<AddressDto> addressesDtoList = addressService.getAddresses(branchOptional);

        assertEquals(addressesDtoList.size(), addresses.size());
        assertEquals(2, addressesDtoList.size());
        assertEquals(addressesDtoList.get(1).getUuid(), addresses.get(1).getUuid());
    }

    @Test
    void shouldGetAddressesForPower() {
        Optional<String> branchOptional = Optional.of("S");
        List<TblAddress> addresses = MockDataHelper.mockAddressList();
        when(addressRepository.findByPowerConnection(true)).thenReturn(addresses);
        List<AddressDto> addressesDtoList = addressService.getAddresses(branchOptional);

        assertEquals(addressesDtoList.size(), addresses.size());
        assertEquals(2, addressesDtoList.size());
        assertEquals(addressesDtoList.get(1).getUuid(), addresses.get(1).getUuid());
    }

    @Test
    void shouldGetAddressesForWater() {
        Optional<String> branchOptional = Optional.of("W");
        List<TblAddress> addresses = MockDataHelper.mockAddressList();
        when(addressRepository.findByWaterConnection(true)).thenReturn(addresses);
        List<AddressDto> addressesDtoList = addressService.getAddresses(branchOptional);

        assertEquals(addressesDtoList.size(), addresses.size());
        assertEquals(2, addressesDtoList.size());
        assertEquals(addressesDtoList.get(1).getUuid(), addresses.get(1).getUuid());
    }

    @Test
    void shouldGetAddressesForGas() {
        Optional<String> branchOptional = Optional.of("G");
        List<TblAddress> addresses = MockDataHelper.mockAddressList();
        when(addressRepository.findByGasConnection(true)).thenReturn(addresses);
        List<AddressDto> addressesDtoList = addressService.getAddresses(branchOptional);

        assertEquals(addressesDtoList.size(), addresses.size());
        assertEquals(2, addressesDtoList.size());
        assertEquals(addressesDtoList.get(1).getUuid(), addresses.get(1).getUuid());
    }

    @Test
    void shouldGetAddressesForFw() {
        Optional<String> branchOptional = Optional.of("F");
        List<TblAddress> addresses = MockDataHelper.mockAddressList();
        when(addressRepository.findByDistrictheatingConnection(true)).thenReturn(addresses);
        List<AddressDto> addressesDtoList = addressService.getAddresses(branchOptional);

        assertEquals(addressesDtoList.size(), addresses.size());
        assertEquals(2, addressesDtoList.size());
        assertEquals(addressesDtoList.get(1).getUuid(), addresses.get(1).getUuid());
    }

    @Test
    void shouldGetAddressesForTk() {
        Optional<String> branchOptional = Optional.of("TK");
        List<TblAddress> addresses = MockDataHelper.mockAddressList();
        when(addressRepository.findByTelecommConnection(true)).thenReturn(addresses);
        List<AddressDto> addressesDtoList = addressService.getAddresses(branchOptional);

        assertEquals(addressesDtoList.size(), addresses.size());
        assertEquals(2, addressesDtoList.size());
        assertEquals(addressesDtoList.get(1).getUuid(), addresses.get(1).getUuid());
    }

    @Test
    void shouldGetAllAddressesForOtherBranches() {
        Optional<String> branchOptional = Optional.of("XXX");
        List<TblAddress> addresses = MockDataHelper.mockAddressList();
        when(addressRepository.findAll()).thenReturn(addresses);
        List<AddressDto> addressesDtoList = addressService.getAddresses(branchOptional);

        assertEquals(addressesDtoList.size(), addresses.size());
        assertEquals(2, addressesDtoList.size());
        assertEquals(addressesDtoList.get(1).getUuid(), addresses.get(1).getUuid());
    }

    @Test
    void shouldGetCommunitysNoParam() {
    Optional<String> branchOptional = Optional.empty();
    List<String> strings = MockDataHelper.mockStringList();
    when(addressRepository.findAllCommunitys()).thenReturn(strings);

    List<String> communities = addressService.getCommunities(branchOptional);

    assertEquals(communities.size(), strings.size());
    assertEquals(2, communities.size());
    assertEquals(communities.get(1), strings.get(1));
}

    @Test
    void shouldGetCommunitysForPowerNoParam() {
        Optional<String> branchOptional = Optional.of("S");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findAllCommunitysForPower()).thenReturn(strings);

        List<String> communities = addressService.getCommunities(branchOptional);

        assertEquals(communities.size(), strings.size());
        assertEquals(2, communities.size());
        assertEquals(communities.get(1), strings.get(1));
    }

    @Test
    void shouldGetCommunitysForGasNoParam() {
        Optional<String> branchOptional = Optional.of("G");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findAllCommunitysForGas()).thenReturn(strings);

        List<String> communities = addressService.getCommunities(branchOptional);

        assertEquals(communities.size(), strings.size());
        assertEquals(2, communities.size());
        assertEquals(communities.get(1), strings.get(1));
    }

    @Test
    void shouldGetCommunitysForWaterNoParam() {
        Optional<String> branchOptional = Optional.of("W");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findAllCommunitysForWater()).thenReturn(strings);

        List<String> communities = addressService.getCommunities(branchOptional);

        assertEquals(communities.size(), strings.size());
        assertEquals(2, communities.size());
        assertEquals(communities.get(1), strings.get(1));
    }

    @Test
    void shouldGetCommunitysForFWNoParam() {
        Optional<String> branchOptional = Optional.of("F");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findAllCommunitysForDistrictheating()).thenReturn(strings);

        List<String> communities = addressService.getCommunities(branchOptional);

        assertEquals(communities.size(), strings.size());
        assertEquals(2, communities.size());
        assertEquals(communities.get(1), strings.get(1));
    }


    @Test
    void shouldGetCommunitysForTkNoParam() {
        Optional<String> branchOptional = Optional.of("TK");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findAllCommunitysForTelecomm()).thenReturn(strings);

        List<String> communities = addressService.getCommunities(branchOptional);

        assertEquals(communities.size(), strings.size());
        assertEquals(2, communities.size());
        assertEquals(communities.get(1), strings.get(1));
    }

    @Test
    void shouldGetCommunitysForOtherBranchesNoParam() {
        Optional<String> branchOptional = Optional.of("GG");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findAllCommunitys()).thenReturn(strings);

        List<String> communities = addressService.getCommunities(branchOptional);

        assertEquals(communities.size(), strings.size());
        assertEquals(2, communities.size());
        assertEquals(communities.get(1), strings.get(1));
    }

    @Test
    void shouldGetPostcodes() {
        Optional<String> branchOptional = Optional.empty();
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findAllPostcodes()).thenReturn(strings);

        List<String> postCodes = addressService.getPostcodes(branchOptional);

        assertEquals(postCodes.size(), strings.size());
        assertEquals(2, postCodes.size());
        assertEquals(postCodes.get(1), strings.get(1));
    }

    @Test
    void shouldGetPostcodesForPower() {
        Optional<String> branchOptional = Optional.of("S");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findAllPostcodesForPower()).thenReturn(strings);

        List<String> postCodes = addressService.getPostcodes(branchOptional);

        assertEquals(postCodes.size(), strings.size());
        assertEquals(2, postCodes.size());
        assertEquals(postCodes.get(1), strings.get(1));
    }

    @Test
    void shouldGetPostcodesForGas() {
        Optional<String> branchOptional = Optional.of("G");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findAllPostcodesForGas()).thenReturn(strings);

        List<String> postCodes = addressService.getPostcodes(branchOptional);

        assertEquals(postCodes.size(), strings.size());
        assertEquals(2, postCodes.size());
        assertEquals(postCodes.get(1), strings.get(1));
    }

    @Test
    void shouldGetPostcodesForWater() {
        Optional<String> branchOptional = Optional.of("W");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findAllPostcodesForWater()).thenReturn(strings);

        List<String> postCodes = addressService.getPostcodes(branchOptional);

        assertEquals(postCodes.size(), strings.size());
        assertEquals(2, postCodes.size());
        assertEquals(postCodes.get(1), strings.get(1));
    }

    @Test
    void shouldGetPostcodesForFW() {
        Optional<String> branchOptional = Optional.of("F");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findAllPostcodesForDistrictheating()).thenReturn(strings);

        List<String> postCodes = addressService.getPostcodes(branchOptional);

        assertEquals(postCodes.size(), strings.size());
        assertEquals(2, postCodes.size());
        assertEquals(postCodes.get(1), strings.get(1));
    }


    @Test
    void shouldGetPostcodesForTk() {
        Optional<String> branchOptional = Optional.of("TK");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findAllPostcodesForTelecomm()).thenReturn(strings);

        List<String> postCodes = addressService.getPostcodes(branchOptional);

        assertEquals(postCodes.size(), strings.size());
        assertEquals(2, postCodes.size());
        assertEquals(postCodes.get(1), strings.get(1));
    }

    @Test
    void shouldGetPostcodesForOtherBranches() {
        Optional<String> branchOptional = Optional.of("GG");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findAllPostcodes()).thenReturn(strings);

        List<String> postCodes = addressService.getPostcodes(branchOptional);

        assertEquals(postCodes.size(), strings.size());
        assertEquals(2, postCodes.size());
        assertEquals(postCodes.get(1), strings.get(1));
    }

    @Test
    void shouldGetCommunities() {
        String postcode = "12345";
        Optional<String> branchOptional = Optional.empty();
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findCommunitiesByPostcode(postcode)).thenReturn(strings);
        List<String> communities = addressService.getCommunities(postcode, branchOptional);

        assertEquals(communities.size(), strings.size());
        assertEquals(2, communities.size());
        assertEquals(communities.get(1), strings.get(1));
    }

    @Test
    void shouldGetCommunitiesForPower() {
        String postcode = "12345";
        Optional<String> branchOptional = Optional.of("S");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findCommunitiesByPostcodeForPowerConnections(postcode)).thenReturn(strings);
        List<String> communities = addressService.getCommunities(postcode, branchOptional);

        assertEquals(communities.size(), strings.size());
        assertEquals(2, communities.size());
        assertEquals(communities.get(1), strings.get(1));
    }

    @Test
    void shouldGetCommunitiesForGas() {
        String postcode = "12345";
        Optional<String> branchOptional = Optional.of("G");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findCommunitiesByPostcodeForGasConnections(postcode)).thenReturn(strings);
        List<String> communities = addressService.getCommunities(postcode, branchOptional);

        assertEquals(communities.size(), strings.size());
        assertEquals(2, communities.size());
        assertEquals(communities.get(1), strings.get(1));
    }

    @Test
    void shouldGetCommunitiesForWater() {
        String postcode = "12345";
        Optional<String> branchOptional = Optional.of("W");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findCommunitiesByPostcodeForWaterConnections(postcode)).thenReturn(strings);
        List<String> communities = addressService.getCommunities(postcode, branchOptional);

        assertEquals(communities.size(), strings.size());
        assertEquals(2, communities.size());
        assertEquals(communities.get(1), strings.get(1));
    }
    @Test
    void shouldGetCommunitiesForFW() {
        String postcode = "12345";
        Optional<String> branchOptional = Optional.of("F");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findCommunitiesByPostcodeForDistrictheatingConnections(postcode)).thenReturn(strings);
        List<String> communities = addressService.getCommunities(postcode, branchOptional);

        assertEquals(communities.size(), strings.size());
        assertEquals(2, communities.size());
        assertEquals(communities.get(1), strings.get(1));
    }

    @Test
    void shouldGetCommunitiesForTK() {
        String postcode = "12345";
        Optional<String> branchOptional = Optional.of("TK");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findCommunitiesByPostcodeForTelecommConnections(postcode)).thenReturn(strings);
        List<String> communities = addressService.getCommunities(postcode, branchOptional);

        assertEquals(communities.size(), strings.size());
        assertEquals(2, communities.size());
        assertEquals(communities.get(1), strings.get(1));
    }

    @Test
    void shouldGetCommunitiesForOtherBranches() {
        String postcode = "12345";
        Optional<String> branchOptional = Optional.of("ST");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findCommunitiesByPostcode(postcode)).thenReturn(strings);
        List<String> communities = addressService.getCommunities(postcode, branchOptional);

        assertEquals(communities.size(), strings.size());
        assertEquals(2, communities.size());
        assertEquals(communities.get(1), strings.get(1));
    }

    @Test
    void shouldGetDistrictsCommOnly() {
        String community = "com1";
        Optional<String> branchOptional = Optional.empty();
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findDistrictsByCommunity(community)).thenReturn(strings);
        List<String> districts = addressService.getDistricts(community, branchOptional);

        assertEquals(districts.size(), strings.size());
        assertEquals(2, districts.size());
        assertEquals(districts.get(1), strings.get(1));
    }

    @Test
    void shouldGetDistrictsForPowerCommOnly() {
        String community = "com1";
        Optional<String> branchOptional = Optional.of("S");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findDistrictsByCommunityForPowerConnections(community)).thenReturn(strings);
        List<String> districts = addressService.getDistricts(community, branchOptional);

        assertEquals(districts.size(), strings.size());
        assertEquals(2, districts.size());
        assertEquals(districts.get(1), strings.get(1));
    }

    @Test
    void shouldGetDistrictsForGasCommOnly() {
        String community = "com1";
        Optional<String> branchOptional = Optional.of("G");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findDistrictsByCommunityForGasConnections(community)).thenReturn(strings);
        List<String> districts = addressService.getDistricts(community, branchOptional);

        assertEquals(districts.size(), strings.size());
        assertEquals(2, districts.size());
        assertEquals(districts.get(1), strings.get(1));
    }

    @Test
    void shouldGetDistrictsForWaterCommOnly() {
        String community = "com1";
        Optional<String> branchOptional = Optional.of("W");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findDistrictsByCommunityForWaterConnections(community)).thenReturn(strings);
        List<String> districts = addressService.getDistricts(community, branchOptional);

        assertEquals(districts.size(), strings.size());
        assertEquals(2, districts.size());
        assertEquals(districts.get(1), strings.get(1));
    }

    @Test
    void shouldGetDistrictsForFwCommOnly() {
        String community = "com1";
        Optional<String> branchOptional = Optional.of("F");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findDistrictsByCommunityForDistrictheatingConnections(community)).thenReturn(strings);
        List<String> districts = addressService.getDistricts(community, branchOptional);

        assertEquals(districts.size(), strings.size());
        assertEquals(2, districts.size());
        assertEquals(districts.get(1), strings.get(1));
    }

    @Test
    void shouldGetDistrictsForTKCommOnly() {
        String community = "com1";
        Optional<String> branchOptional = Optional.of("TK");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findDistrictsByCommunityForTelecommConnections(community)).thenReturn(strings);
        List<String> districts = addressService.getDistricts(community, branchOptional);

        assertEquals(districts.size(), strings.size());
        assertEquals(2, districts.size());
        assertEquals(districts.get(1), strings.get(1));
    }

    @Test
    void shouldGetDistrictsForOtherBranchesCommOnly() {
        String community = "com1";
        Optional<String> branchOptional = Optional.of("GG");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findDistrictsByCommunity(community)).thenReturn(strings);
        List<String> districts = addressService.getDistricts(community, branchOptional);

        assertEquals(districts.size(), strings.size());
        assertEquals(2, districts.size());
        assertEquals(districts.get(1), strings.get(1));
    }

    @Test
    void shouldGetDistricts() {
        String postcode = "12345";
        String community = "com1";
        Optional<String> branchOptional = Optional.empty();
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findDistrictsByPostcodeAndCommunity(postcode, community)).thenReturn(strings);
        List<String> districts = addressService.getDistricts(postcode, community, branchOptional);

        assertEquals(districts.size(), strings.size());
        assertEquals(2, districts.size());
        assertEquals(districts.get(1), strings.get(1));
    }

    @Test
    void shouldGetDistrictsForPower() {
        String postcode = "12345";
        String community = "com1";
        Optional<String> branchOptional = Optional.of("S");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findDistrictsByPostcodeAndCommunityForPower(postcode, community)).thenReturn(strings);
        List<String> districts = addressService.getDistricts(postcode, community, branchOptional);

        assertEquals(districts.size(), strings.size());
        assertEquals(2, districts.size());
        assertEquals(districts.get(1), strings.get(1));
    }

    @Test
    void shouldGetDistrictsForGas() {
        String postcode = "12345";
        String community = "com1";
        Optional<String> branchOptional = Optional.of("G");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findDistrictsByPostcodeAndCommunityForGas(postcode, community)).thenReturn(strings);
        List<String> districts = addressService.getDistricts(postcode, community, branchOptional);

        assertEquals(districts.size(), strings.size());
        assertEquals(2, districts.size());
        assertEquals(districts.get(1), strings.get(1));
    }

    @Test
    void shouldGetDistrictsForWater() {
        String postcode = "12345";
        String community = "com1";
        Optional<String> branchOptional = Optional.of("W");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findDistrictsByPostcodeAndCommunityForWater(postcode, community)).thenReturn(strings);
        List<String> districts = addressService.getDistricts(postcode, community, branchOptional);

        assertEquals(districts.size(), strings.size());
        assertEquals(2, districts.size());
        assertEquals(districts.get(1), strings.get(1));
    }

    @Test
    void shouldGetDistrictsForFw() {
        String postcode = "12345";
        String community = "com1";
        Optional<String> branchOptional = Optional.of("F");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findDistrictsByPostcodeAndCommunityForDistrictheating(postcode, community)).thenReturn(strings);
        List<String> districts = addressService.getDistricts(postcode, community, branchOptional);

        assertEquals(districts.size(), strings.size());
        assertEquals(2, districts.size());
        assertEquals(districts.get(1), strings.get(1));
    }

    @Test
    void shouldGetDistrictsForTK() {
        String postcode = "12345";
        String community = "com1";
        Optional<String> branchOptional = Optional.of("TK");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findDistrictsByPostcodeAndCommunityForTelecomm(postcode, community)).thenReturn(strings);
        List<String> districts = addressService.getDistricts(postcode, community, branchOptional);

        assertEquals(districts.size(), strings.size());
        assertEquals(2, districts.size());
        assertEquals(districts.get(1), strings.get(1));
    }

    @Test
    void shouldGetDistrictsForOtherBranches() {
        String postcode = "12345";
        String community = "com1";
        Optional<String> branchOptional = Optional.of("GG");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findDistrictsByPostcodeAndCommunity(postcode, community)).thenReturn(strings);
        List<String> districts = addressService.getDistricts(postcode, community, branchOptional);

        assertEquals(districts.size(), strings.size());
        assertEquals(2, districts.size());
        assertEquals(districts.get(1), strings.get(1));
    }

    @Test
    void shouldGetPostcodesTwoParams() {
        String community = "12345";
        String district = "com1";
        Optional<String> branchOptional = Optional.empty();
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findPostcodesByCommunityAndDistrict(community, district)).thenReturn(strings);
        List<String> districts = addressService.getPostcodes(community, district, branchOptional);

        assertEquals(districts.size(), strings.size());
        assertEquals(2, districts.size());
        assertEquals(districts.get(1), strings.get(1));
    }

    @Test
    void shouldGetPostcodesForPowerTwoParams() {
        String community = "12345";
        String district = "com1";
        Optional<String> branchOptional = Optional.of("S");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findPostcodesByCommunityAndDistrictForPower(community, district)).thenReturn(strings);
        List<String> districts = addressService.getPostcodes(community, district, branchOptional);

        assertEquals(districts.size(), strings.size());
        assertEquals(2, districts.size());
        assertEquals(districts.get(1), strings.get(1));
    }

    @Test
    void shouldGetPostcodesForGasTwoParams() {
        String community = "12345";
        String district = "com1";
        Optional<String> branchOptional = Optional.of("G");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findPostcodesByCommunityAndDistrictForGas(community, district)).thenReturn(strings);
        List<String> districts = addressService.getPostcodes(community, district, branchOptional);

        assertEquals(districts.size(), strings.size());
        assertEquals(2, districts.size());
        assertEquals(districts.get(1), strings.get(1));
    }

    @Test
    void shouldGetPostcodesForWaterTwoParams() {
        String community = "12345";
        String district = "com1";
        Optional<String> branchOptional = Optional.of("W");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findPostcodesByCommunityAndDistrictForWater(community, district)).thenReturn(strings);
        List<String> districts = addressService.getPostcodes(community, district, branchOptional);

        assertEquals(districts.size(), strings.size());
        assertEquals(2, districts.size());
        assertEquals(districts.get(1), strings.get(1));
    }

    @Test
    void shouldGetPostcodesForFwTwoParams() {
        String community = "12345";
        String district = "com1";
        Optional<String> branchOptional = Optional.of("F");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findPostcodesByCommunityAndDistrictForDistrictheating(community, district)).thenReturn(strings);
        List<String> districts = addressService.getPostcodes(community, district, branchOptional);

        assertEquals(districts.size(), strings.size());
        assertEquals(2, districts.size());
        assertEquals(districts.get(1), strings.get(1));
    }

    @Test
    void shouldGetPostcodesForTKTwoParams() {
        String community = "12345";
        String district = "com1";
        Optional<String> branchOptional = Optional.of("TK");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findPostcodesByCommunityAndDistrictForTelecomm(community, district)).thenReturn(strings);
        List<String> districts = addressService.getPostcodes(community, district, branchOptional);

        assertEquals(districts.size(), strings.size());
        assertEquals(2, districts.size());
        assertEquals(districts.get(1), strings.get(1));
    }

    @Test
    void shouldGetPostcodesForOtherBranchesTwoParams() {
        String community = "12345";
        String district = "com1";
        Optional<String> branchOptional = Optional.of("GG");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findPostcodesByCommunityAndDistrict(community, district)).thenReturn(strings);
        List<String> districts = addressService.getPostcodes(community, district, branchOptional);

        assertEquals(districts.size(), strings.size());
        assertEquals(2, districts.size());
        assertEquals(districts.get(1), strings.get(1));
    }

    @Test
    void shouldGetHousenumbers() {
        String postcode = "12345";
        String community = "com1";
        String street = "street1";
        Optional<String> branchOptional = Optional.empty();
        List<HousenumberUuidDto> housenumberUuids = MockDataHelper.mockHousnumberUuidList();
        List<TblAddress> tblAddresses = MockDataHelper.mockAddressList();

        when(addressRepository.findByPostcodeAndCommunityAndStreet(postcode, community, street)).thenReturn(tblAddresses);

        List<HousenumberUuidDto> housenumbers = addressService.getHousenumbers(postcode, community, street, branchOptional);

        assertEquals(housenumbers.size(), housenumberUuids.size());
        assertEquals(2, housenumbers.size());
        assertEquals(housenumbers.get(1).getHousenumber(), housenumberUuids.get(1).getHousenumber());
    }

    @Test
    void shouldGetHousenumbersForPower() {
        String postcode = "12345";
        String community = "com1";
        String street = "street1";
        Optional<String> branchOptional = Optional.of("S");
        List<HousenumberUuidDto> housenumberUuids = MockDataHelper.mockHousnumberUuidList();
        List<TblAddress> tblAddresses = MockDataHelper.mockAddressList();

        when(addressRepository.findByPostcodeAndCommunityAndStreetForPower(postcode, community, street)).thenReturn(tblAddresses);

        List<HousenumberUuidDto> housenumbers = addressService.getHousenumbers(postcode, community, street, branchOptional);

        assertEquals(housenumbers.size(), housenumberUuids.size());
        assertEquals(2, housenumbers.size());
        assertEquals(housenumbers.get(1).getHousenumber(), housenumberUuids.get(1).getHousenumber());
    }

    @Test
    void shouldGetHousenumbersForGas() {
        String postcode = "12345";
        String community = "com1";
        String street = "street1";
        Optional<String> branchOptional = Optional.of("G");
        List<HousenumberUuidDto> housenumberUuids = MockDataHelper.mockHousnumberUuidList();
        List<TblAddress> tblAddresses = MockDataHelper.mockAddressList();

        when(addressRepository.findByPostcodeAndCommunityAndStreetForGas(postcode, community, street)).thenReturn(tblAddresses);

        List<HousenumberUuidDto> housenumbers = addressService.getHousenumbers(postcode, community, street, branchOptional);

        assertEquals(housenumbers.size(), housenumberUuids.size());
        assertEquals(2, housenumbers.size());
        assertEquals(housenumbers.get(1).getHousenumber(), housenumberUuids.get(1).getHousenumber());
    }

    @Test
    void shouldGetHousenumbersForWater() {
        String postcode = "12345";
        String community = "com1";
        String street = "street1";
        Optional<String> branchOptional = Optional.of("W");
        List<HousenumberUuidDto> housenumberUuids = MockDataHelper.mockHousnumberUuidList();
        List<TblAddress> tblAddresses = MockDataHelper.mockAddressList();

        when(addressRepository.findByPostcodeAndCommunityAndStreetForWater(postcode, community, street)).thenReturn(tblAddresses);

        List<HousenumberUuidDto> housenumbers = addressService.getHousenumbers(postcode, community, street, branchOptional);

        assertEquals(housenumbers.size(), housenumberUuids.size());
        assertEquals(2, housenumbers.size());
        assertEquals(housenumbers.get(1).getHousenumber(), housenumberUuids.get(1).getHousenumber());
    }

    @Test
    void shouldGetHousenumbersForFW() {
        String postcode = "12345";
        String community = "com1";
        String street = "street1";
        Optional<String> branchOptional = Optional.of("F");
        List<HousenumberUuidDto> housenumberUuids = MockDataHelper.mockHousnumberUuidList();
        List<TblAddress> tblAddresses = MockDataHelper.mockAddressList();

        when(addressRepository.findByPostcodeAndCommunityAndStreetForDistrictheating(postcode, community, street)).thenReturn(tblAddresses);

        List<HousenumberUuidDto> housenumbers = addressService.getHousenumbers(postcode, community, street, branchOptional);

        assertEquals(housenumbers.size(), housenumberUuids.size());
        assertEquals(2, housenumbers.size());
        assertEquals(housenumbers.get(1).getHousenumber(), housenumberUuids.get(1).getHousenumber());
    }

    @Test
    void shouldGetHousenumbersForTelecomm() {
        String postcode = "12345";
        String community = "com1";
        String street = "street1";
        Optional<String> branchOptional = Optional.of("TK");
        List<HousenumberUuidDto> housenumberUuids = MockDataHelper.mockHousnumberUuidList();
        List<TblAddress> tblAddresses = MockDataHelper.mockAddressList();

        when(addressRepository.findByPostcodeAndCommunityAndStreetForTelecomm(postcode, community, street)).thenReturn(tblAddresses);

        List<HousenumberUuidDto> housenumbers = addressService.getHousenumbers(postcode, community, street, branchOptional);

        assertEquals(housenumbers.size(), housenumberUuids.size());
        assertEquals(2, housenumbers.size());
        assertEquals(housenumbers.get(1).getHousenumber(), housenumberUuids.get(1).getHousenumber());
    }

    @Test
    void shouldGetHousenumbersForOtherBranches() {
        String postcode = "12345";
        String community = "com1";
        String street = "street1";
        Optional<String> branchOptional = Optional.of("S");
        List<HousenumberUuidDto> housenumberUuids = MockDataHelper.mockHousnumberUuidList();
        List<TblAddress> tblAddresses = MockDataHelper.mockAddressList();

        when(addressRepository.findByPostcodeAndCommunityAndStreetForPower(postcode, community, street)).thenReturn(tblAddresses);

        List<HousenumberUuidDto> housenumbers = addressService.getHousenumbers(postcode, community, street, branchOptional);

        assertEquals(housenumbers.size(), housenumberUuids.size());
        assertEquals(2, housenumbers.size());
        assertEquals(housenumbers.get(1).getHousenumber(), housenumberUuids.get(1).getHousenumber());
    }

    @Test
    void shouldGetStreetsWithoutDistricts() {
        String postcode = "12345";
        String community = "com1";
        Optional<String> district = Optional.empty();
        Optional<String> branchOptional = Optional.empty();
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findStreetsByPostcodeAndCommunity(postcode, community)).thenReturn(strings);
        List<String> streets = addressService.getStreets(postcode, community, district, branchOptional);

        assertEquals(streets.size(), strings.size());
        assertEquals(2, streets.size());
        assertEquals(streets.get(1), strings.get(1));
    }

    @Test
    void shouldGetStreetsWithoutDistrictsForPower() {
        String postcode = "12345";
        String community = "com1";
        Optional<String> district = Optional.empty();
        Optional<String> branchOptional = Optional.of("S");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findStreetsByPostcodeAndCommunityForPower(postcode, community)).thenReturn(strings);
        List<String> streets = addressService.getStreets(postcode, community, district, branchOptional);

        assertEquals(streets.size(), strings.size());
        assertEquals(2, streets.size());
        assertEquals(streets.get(1), strings.get(1));
    }

    @Test
    void shouldGetStreetsWithoutDistrictsForGas() {
        String postcode = "12345";
        String community = "com1";
        Optional<String> district = Optional.empty();
        Optional<String> branchOptional = Optional.of("G");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findStreetsByPostcodeAndCommunityForGas(postcode, community)).thenReturn(strings);
        List<String> streets = addressService.getStreets(postcode, community, district, branchOptional);

        assertEquals(streets.size(), strings.size());
        assertEquals(2, streets.size());
        assertEquals(streets.get(1), strings.get(1));
    }

    @Test
    void shouldGetStreetsWithoutDistrictsForWater() {
        String postcode = "12345";
        String community = "com1";
        Optional<String> district = Optional.empty();
        Optional<String> branchOptional = Optional.of("W");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findStreetsByPostcodeAndCommunityForWater(postcode, community)).thenReturn(strings);
        List<String> streets = addressService.getStreets(postcode, community, district, branchOptional);

        assertEquals(streets.size(), strings.size());
        assertEquals(2, streets.size());
        assertEquals(streets.get(1), strings.get(1));
    }

    @Test
    void shouldGetStreetsWithoutDistrictsForFW() {
        String postcode = "12345";
        String community = "com1";
        Optional<String> district = Optional.empty();
        Optional<String> branchOptional = Optional.of("F");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findStreetsByPostcodeAndCommunityForDistrictheating(postcode, community)).thenReturn(strings);
        List<String> streets = addressService.getStreets(postcode, community, district, branchOptional);

        assertEquals(streets.size(), strings.size());
        assertEquals(2, streets.size());
        assertEquals(streets.get(1), strings.get(1));
    }
    @Test
    void shouldGetStreetsWithoutDistrictsForTk() {
        String postcode = "12345";
        String community = "com1";
        Optional<String> district = Optional.empty();
        Optional<String> branchOptional = Optional.of("TK");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findStreetsByPostcodeAndCommunityForTelecomm(postcode, community)).thenReturn(strings);
        List<String> streets = addressService.getStreets(postcode, community, district, branchOptional);

        assertEquals(streets.size(), strings.size());
        assertEquals(2, streets.size());
        assertEquals(streets.get(1), strings.get(1));
    }

    @Test
    void shouldGetStreetsWithoutDistrictsForOther() {
        String postcode = "12345";
        String community = "com1";
        Optional<String> district = Optional.empty();
        Optional<String> branchOptional = Optional.of("ST");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findStreetsByPostcodeAndCommunity(postcode, community)).thenReturn(strings);
        List<String> streets = addressService.getStreets(postcode, community, district, branchOptional);

        assertEquals(streets.size(), strings.size());
        assertEquals(2, streets.size());
        assertEquals(streets.get(1), strings.get(1));
    }

    @Test
    void shouldGetStreetsWithDistricts() {
        String postcode = "12345";
        String community = "com1";
        String district = "test1";
        Optional<String> optionalDistrict = Optional.of("test1");
        Optional<String> branchOptional = Optional.empty();
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findStreetsByPostcodeAndCommunityAndDistrict(postcode, community, district)).thenReturn(strings);
        List<String> streets = addressService.getStreets(postcode, community, optionalDistrict, branchOptional);

        assertEquals(streets.size(), strings.size());
        assertEquals(2, streets.size());
        assertEquals(streets.get(1), strings.get(1));
    }

    @Test
    void shouldGetStreetsWithDistrictsForPower() {
        String postcode = "12345";
        String community = "com1";
        String district = "test1";
        Optional<String> optionalDistrict = Optional.of("test1");
        Optional<String> branchOptional = Optional.of("S");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findStreetsByPostcodeAndCommunityAndDistrictForPower(postcode, community, district)).thenReturn(strings);
        List<String> streets = addressService.getStreets(postcode, community, optionalDistrict, branchOptional);

        assertEquals(streets.size(), strings.size());
        assertEquals(2, streets.size());
        assertEquals(streets.get(1), strings.get(1));
    }

    @Test
    void shouldGetStreetsWithDistrictsForGas() {
        String postcode = "12345";
        String community = "com1";
        String district = "test1";
        Optional<String> optionalDistrict = Optional.of("test1");
        Optional<String> branchOptional = Optional.of("G");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findStreetsByPostcodeAndCommunityAndDistrictForGas(postcode, community, district)).thenReturn(strings);
        List<String> streets = addressService.getStreets(postcode, community, optionalDistrict, branchOptional);

        assertEquals(streets.size(), strings.size());
        assertEquals(2, streets.size());
        assertEquals(streets.get(1), strings.get(1));
    }

    @Test
    void shouldGetStreetsWithDistrictsForWater() {
        String postcode = "12345";
        String community = "com1";
        String district = "test1";
        Optional<String> optionalDistrict = Optional.of("test1");
        Optional<String> branchOptional = Optional.of("W");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findStreetsByPostcodeAndCommunityAndDistrictForWater(postcode, community, district)).thenReturn(strings);
        List<String> streets = addressService.getStreets(postcode, community, optionalDistrict, branchOptional);

        assertEquals(streets.size(), strings.size());
        assertEquals(2, streets.size());
        assertEquals(streets.get(1), strings.get(1));
    }

    @Test
    void shouldGetStreetsWithDistrictsForFw() {
        String postcode = "12345";
        String community = "com1";
        String district = "test1";
        Optional<String> optionalDistrict = Optional.of("test1");
        Optional<String> branchOptional = Optional.of("F");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findStreetsByPostcodeAndCommunityAndDistrictForDistrictHeating(postcode, community, district)).thenReturn(strings);
        List<String> streets = addressService.getStreets(postcode, community, optionalDistrict, branchOptional);

        assertEquals(streets.size(), strings.size());
        assertEquals(2, streets.size());
        assertEquals(streets.get(1), strings.get(1));
    }

    @Test
    void shouldGetStreetsWithDistrictsForTk() {
        String postcode = "12345";
        String community = "com1";
        String district = "test1";
        Optional<String> optionalDistrict = Optional.of("test1");
        Optional<String> branchOptional = Optional.of("TK");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findStreetsByPostcodeAndCommunityAndDistrictForTelecomm(postcode, community, district)).thenReturn(strings);
        List<String> streets = addressService.getStreets(postcode, community, optionalDistrict, branchOptional);

        assertEquals(streets.size(), strings.size());
        assertEquals(2, streets.size());
        assertEquals(streets.get(1), strings.get(1));
    }

    @Test
    void shouldGetStreetsWithDistrictsForOtherBranches() {
        String postcode = "12345";
        String community = "com1";
        String district = "test1";
        Optional<String> optionalDistrict = Optional.of("test1");
        Optional<String> branchOptional = Optional.of("XYZ");
        List<String> strings = MockDataHelper.mockStringList();
        when(addressRepository.findStreetsByPostcodeAndCommunityAndDistrict(postcode, community, district)).thenReturn(strings);
        List<String> streets = addressService.getStreets(postcode, community, optionalDistrict, branchOptional);

        assertEquals(streets.size(), strings.size());
        assertEquals(2, streets.size());
        assertEquals(streets.get(1), strings.get(1));
    }
}
